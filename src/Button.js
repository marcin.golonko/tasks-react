import React from 'react'

const HtmlButton = (props) => {
    const {label, onButtonClick} = props
    return (
        <div>
            <button onClick={ onButtonClick }>{ label }</button>
        </div>
    )
}

const onClickButton = () => {
    console.log("kliknięto przycisk");
}

const Button = () => {
    return (
        <HtmlButton label={'Wciśnij mnie'} onButtonClick= {onClickButton }/>
    )
}


export default Button
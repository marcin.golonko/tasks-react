import React, { Component } from 'react';
import axios from 'axios'

const ToDoItem = (title) => {
    return (
        <li>
            <h3>{ title }</h3>
        </li>
    )
}

class ToDoList extends Component {
    
    state = {
        imBusy: '',
        todos: [],
    }

    getTodos() {
        axios
            .get(`http://195.181.210.249:3000/todo/`)
            .then(res => {
                const todos = res.data;
                console.log(todos);
                this.setState({todos});
                console.log(this.state.todos)
            })
            .catch((error) => {
                console.log(error);
            })

    }

    componentDidMount(){
        this.getTodos();

    }

    render(){
        return (
            <ul>
                {this.state.todos.map((todo) =>
                    <ToDoItem title={todo}/>
                )}
            </ul>
        )
    }
}

export default ToDoList
import React, { Component } from 'react'

class Input extends Component {
    constructor(props){
        super(props)
        this.state = {
            inputValue:'',
        }
        //this.onInputChange = this.onInputChange.bind(this);
    }
    onInputChange = (event)=>{
        this.setState({inputValue: event.target.value});
        console.log(event.target.value);
        //console.log(this.state.inputValue);
    }
    
    render(){
        return (
            <div>
                <input type= "text" value= {this.state.inputValue} onChange= {this.onInputChange} placeholder= "Wpisz tekst"/>
            </div>
        )
    }
}

export default Input

import React from 'react';
import './App.css';
import Input from './Input.js'
import Button from './Button.js'
import ToDoList from './ToDoList.js'

function App() {
  return (
      <div>
        <Input />
        <Button />
        <ToDoList />
      </div>
  );
}

export default App;
